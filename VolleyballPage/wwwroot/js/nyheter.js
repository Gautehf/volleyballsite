$(document).ready(function() {
    //Iterate through x posts
    for (let i = 1; i < $('.panel').length + 1; i++) {
        $('#savebtn' + i).hide();
        $('#savetxt' + i).hide();

        $('#deletebtn' + i).click(function(){
            DeletePost(i);
        });


        $('#editbtn' + i).click(function() {
            //Enable editing for text and textarea
            $('#msg' + i).prop("disabled", false);
            $('#pan' + i).attr('class', 'panel panel-danger');

            //Adds border
            $('#msg'+i).addClass('msgborder');
            $('#msg'+i).removeClass('msgnoborder');

            //Hides edit, shows save
            $('#edittxt'+i).hide();
            $('#savetxt'+i).show();

            //Hides editbtn onclick, shows save
            $(this).hide();
            $('#savebtn' + i).show();
            // $('[id="'+ 'deletebtn ' + i +'"]').show();

        });

        $('#savebtn' + i).click(function() {

            //Enable editing for text and textarea
            $('#savebtn' + i).prop("disabled", true);
            $('#msg' + i).prop("disabled", true);
            $('#pan' + i).attr('class', 'panel panel-primary');

            //Removes border
            $('#msg'+i).addClass('msgnoborder');
            $('#msg'+i).removeClass('msgborder');

            //Enables savetext
            $('#savetxt'+i).hide();
            $('#edittxt'+i).show();

            //Enable button to click
            $('#savebtn' + i).prop("disabled", false);
            

            //Hides button on click, shows editbtn
            $(this).hide();
            $('#editbtn' + i).show();


            var message = $('#msg' + i).val();

            // POST AJAX
            $.ajax({
                type: 'POST',
                data: { 'name': name, 'message': message, 'id': i },
                url: '/Home/UpdatePost',
                cache: false,
                success: function(result) {
                    window.location.href = result.url;
                }
            });
        });
    };
    $('textarea.expand').focus(function(){
        $(this).animate({ height: "10em" }, 500); 
    }).blur(function(){
        $(this).animate({ height: "3em" }, 500);
    });
});

function DeletePost(id) {
    $.ajax({
        url: "/home/PostDelete",
        type: "DELETE",
        data: {
            'id': id
        },
        async: false,
        success: function(result) {
            window.location.href = result.url;
        }
    });
}