﻿(function () {
    const app = angular.module("app", ["ngMaterial", "ngAria"]); //Bootstrap angular module => include material and aria libraies in this module

    app.controller("userTeamController", //Angular controller
        ($scope, $http, $mdDialog) => { //Stuff you are using in this controller
            //data for ng-repeat
            $scope.names = []; //Dropdown values

            $scope.selected = {}; //selected input value

            $scope.dropdown = {}; //selected drop down value

            $scope.userTeamDetails = {};

            $scope.GetTeamUsers = () => {
                const input = $scope.selected.input; //selected input value
                $http.get("/home/GetTeamUsers?teamUser=" + input)
                    .success((data, status, headers, config) => {
                        // this callback will be called asynchronously
                        // when the response is available
                        $scope.names = {};
                        $scope.names = data;
                    })
                    .error((data, status, headers, config) => {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                        console.log(data);
                        console.log(status);
                        console.log(headers);
                        console.log(config);
                    });
            };

            $scope.ShowTeamUserPopup = (ev) => {
                const model = $scope.selected.input;
                var unique = $scope.names.filter((v, i, a) => a.indexOf(v) === i); 
                unique.forEach((element) => { //Iterate through list of options
                    if (element.value === model) { // Check if input value is equal to one of the options
                        $http.get("/home/GetTeamUsersDetails?teamUser=" + model)
                            .success((data, status, headers, config) => {
                                // this callback will be called asynchronously
                                // when the response is available
                                $scope.userTeamDetails = data; // Get User or Team Details and put it into scope
                                $scope.ShowUser(ev);
                                //$scope.showAdvanced(ev); // Show advance modal ..func below
                            })
                            .error((data, status, headers, config) => {
                                // called asynchronously if an error occurs
                                // or server returns response with an error status.
                                console.log(data);
                                console.log(status);
                                console.log(headers);
                                console.log(config);
                            });
                    } else {
                        //$scope.GetTeamUsers();    //This was required for each key press on autocomplete
                        return;
                    }
                });
            };

            $scope.status = "  ";

            $scope.customFullscreen = false;

            // $scope.showAdvanced
            $scope.ShowUser = (ev) => {
                //var tempUrl = $scope.userTeamDetails.isUser ? "../html/UserDialog.tmpl.html" : "../html/TeamDialog.tmpl.html";
                $mdDialog.show({
                        controller: dialogController,
                        templateUrl: "../html/UserDialog.tmpl.html",
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true,
                        fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                        hasBackdrop: true,
                        locals: {
                            data: $scope.userTeamDetails
                        },
                        bindToController: true,
                        controllerAs: "ctrl"
                    })
                    .then((answer) => {
                            $scope.status = 'You said the information was "' + answer + '".';
                        },
                        () => {
                            $scope.status = "You cancelled the dialog.";
                        });
            };

            function dialogController($scope, $mdDialog) {
                $scope.hide = () => {
                    $mdDialog.hide();//To Hide Popup
                };

                $scope.cancel = () => {
                    $mdDialog.cancel();//To Cancel Popup
                };

                $scope.answer = (answer) => {
                    $mdDialog.hide(answer); //To Answer or respond to something in popup
                };

            }

        });

    app.$inject = ["$scope", "$http", "$mdDialog"]; //This is required if you want to minify angular js
})();