// Write your Javascript code.
$(document).ready(function () {

    //scroll back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
            $("#return-to-top").fadeIn(); // Fade in the arrow
        } else {
            $("#return-to-top").fadeOut(); // Else fade out the arrow
        }
    });
    $("#return-to-top").click(function () { // When arrow is clicked
        $("#return-to-top").tooltip('hide');
        $("body,html").animate({
            scrollTop: 0 // Scroll to top of body
        }, 800);
        return false;
    });
    $("#return-to-top").tooltip('show');



    var navListItems = $("div.setup-panel div a"),
        allWells = $(".setup-content"),
        allNextBtn = $(".nextBtn");

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr("href")),
            $item = $(this);

        if (!$item.hasClass("disabled")) {
            navListItems.removeClass("btn-primary").addClass("btn-default");
            $item.addClass("btn-primary");
            allWells.hide();
            $target.show();
            $target.find("input:eq(0)").focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (let i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr("disabled").trigger("click");
    });

    $("div.setup-panel div a.btn-primary").trigger("click");
});

function EditLag(id) {
    const form = $("#teamForm");
    $.validator.unobtrusive.parse(form);
    form.validate();
    if (form.valid()) {
        const navn = $(`#name-${id}`).val();
        const region = $(`#msg-${id}`).val();


        window.dialog.confirm({
            title: "Confirmation",

            message: "Are you sure ?",

            cancel: "Don't Agree",

            button: "Agree",

            required: true,

            callback: function (value) {
                if (value === true) {
                    $.ajax({
                        url: "/home/LagUpdate",
                        type: "PUT",
                        data: {
                            'id': id,
                            'Navn': navn,
                            'Region': region
                        },
                        async: false,
                        success: function (result) {
                            window.location.href = result.url;
                        }
                    });
                } else {
                    return;
                }
            }
        });
    }
    return false;
}

function EditRegion(id) {
    const form = $("#teamForm");
    $.validator.unobtrusive.parse(form);
    form.validate();
    if (form.valid()) {
        const region = $(`#msg-${id}`).val();


        window.dialog.confirm({
            title: "Confirmation",

            message: "Are you sure ?",

            cancel: "Don't Agree",

            button: "Agree",

            required: true,

            callback: function (value) {
                if (value === true) {
                    $.ajax({
                        url: "/home/RegionUpdate",
                        type: "PUT",
                        data: {
                            'id': id,
                            'RegionNavn': region
                        },
                        async: false,
                        success: function (result) {
                            window.location.href = result.url;
                        }
                    });
                } else {
                    return;
                }
            }
        });
    }
    return false;
}

function DeleteLag(id) {
    $.ajax({
        url: "/home/LagDelete",
        type: "DELETE",
        data: {
            'id': id
        },
        async: false,
        success: function (result) {
            window.location.href = result.url;
        }
    });
}

function DeleteRegion(id) {
    $.ajax({
        url: "/home/RegionDelete",
        type: "DELETE",
        data: {
            'id': id
        },
        async: false,
        success: function (result) {
            window.location.href = result.url;
        }
    });
}

$(function () {
    $("#Region_RegionNavn").on("change", function () {
        $("#SelectedRegion").val($(this).text());
    });
});

$(document).on("change", "#Region", function () {
    const region = this.value;
    $.ajax({
        url: `/home/GetLag/${region}`,
        type: "Get",
        async: false,
        success: function (result) {
            var $el = $("#Lag");
            $el.empty();
            $el.append($("<option></option>")
                .attr("value", "").text("Please Select"));
            $.each(result, function (value, key) {
                $el.append($("<option></option>")
                    .attr("value", key.value).text(key.value));
            });
        }
    });
});

function isNullOrEmpty(s) {
    return s == null || s === "";
}

$(document).on("keyup blur", "#Email,#Password,#ConfirmPassword", function () {
    if (!isNullOrEmpty($("#Email").val()) &&
        !isNullOrEmpty($("#Password").val()) &&
        !isNullOrEmpty($("#ConfirmPassword").val())) { //Sjekker her om verdien i tekstboksene er tomme
        if ($("#Email").valid() && $("#Password").valid() && $("#ConfirmPassword").valid() //Validate all textboxes
        ) { // sjekker skjema for validering
            $("#btnStepOneNext").prop("disabled", false); // Gjør knappen brukbar
        } else {
            $("#btnStepOneNext").prop("disabled", "disabled"); //deaktiverer knappen
        }
    } else {
        $("#btnStepOneNext").prop("disabled", "disabled"); // deaktiverer knappen
    }
});


jQuery("document").ready(function ($) {

    var nav = $("#navbar");

    $(window).scroll(function () {
        if ($(this).scrollTop() > 130) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });

});