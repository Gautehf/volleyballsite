

namespace VolleyballPage.Models
{
    public class Region
    {
        public Region() {}

        public Region(string regionnavn)
        {
            RegionNavn = regionnavn;
        }
        public int Id {get; set;}

        public string RegionNavn {get; set;}

    }
}