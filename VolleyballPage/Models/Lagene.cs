using System.ComponentModel.DataAnnotations;

namespace VolleyballPage.Models
{
    public class Lag
    {

        public Lag() {}
        public Lag(string navn, string region)
        {
            Navn = navn;
            Region = region;
        }

        public int Id { get; set; }
        
        [Required]
        public string Navn { get; set; }
        
        public string Region { get; set; }

    }
}