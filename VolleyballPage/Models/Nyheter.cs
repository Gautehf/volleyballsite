namespace VolleyballPage.Models
{
    public class Nyheter
    {

        public Nyheter() { }

        public Nyheter(string postTitle, string message, string email)
        {
            this.PostTitle = postTitle;
            this.Message = message;
            this.Email = email;
        }

        public int Id {get; set;}

        public string Email{get; set;}

        public string PostTitle { get; set; }

        public string Message { get; set; }

    }
}