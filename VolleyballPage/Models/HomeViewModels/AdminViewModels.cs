using System.Collections.Generic;
using VolleyballPage.Models.AccountViewModels;

namespace VolleyballPage.Models.HomeViewModels
{
    public class AdminViewModel
    {
        public Lag Lag { get; set; }
        public Region Region { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        public IEnumerable<Region> Regions { get; set; }
    }
}
