using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using VolleyballPage.Models;
using VolleyballPage.Models.HomeViewModels;
using System.Collections.Generic;

namespace VolleyballPage.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Naming conventions
        /// Private variables start with _ and are camel cased
        /// Parameter variables are camel cased
        /// </summary>
        private UserManager<ApplicationUser> _um;

        private readonly Data.ApplicationDbContext _db;

        private RoleManager<IdentityRole> _rm;

        public HomeController(Data.ApplicationDbContext db, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _db = db;
            _um = userManager;
            _rm = roleManager;
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Contact()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult KontaktOss()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Admin()
        {
            var regions = _db.Regionene.ToList();
            var viewModel = new AdminViewModel { Regions = regions };
            return View(viewModel);

        }

        /// <summary>
        ///  Only Lag navn is bind is through textbox and region id is bind thorugh ddl
        ///  So I query db to get region by id
        ///  then supply remaining lag values
        ///  Add lagene and supply drop down values
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Admin(AdminViewModel vm)
        {
            var region = _db.Regionene.Find(vm.Region.Id);
            vm.Lag.Region = region.RegionNavn;

            if (ModelState.IsValid)
            {
                _db.Lagene.Add(vm.Lag);
                _db.SaveChanges();
            }
            var regions = _db.Regionene.ToList();
            var viewModel = new AdminViewModel { Regions = regions };
            return View(viewModel);
        }

        /// <summary>
        ///  Only Lag navn is bind is through textbox and region id is bind thorugh ddl
        ///  So I query db to get region by id
        ///  then supply remaining lag values
        ///  Add lagene and supply drop down values
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Region(AdminViewModel vm)
        {
            if (ModelState.IsValid)
            {
                _db.Regionene.Add(vm.Region);
                _db.SaveChanges();
            }
            var regions = _db.Regionene.ToList();
            var viewModel = new AdminViewModel { Regions = regions };
            return View("Admin", viewModel);
        }

        [HttpPut]
        public IActionResult LagUpdate(Lag lagene)
        {
            _db.Lagene.Update(lagene);
            _db.SaveChanges();

            var redirectUrl = Url.Action("Admin", "Home");
            return Json(new { Url = redirectUrl });
        }

        [HttpDelete]
        public IActionResult LagDelete(int id)
        {
            var lag = _db.Lagene.Find(id);
            _db.Lagene.Remove(lag);
            _db.SaveChanges();

            var redirectUrl = Url.Action("Admin", "Home");
            return Json(new { Url = redirectUrl });
        }

        [HttpPut]
        public IActionResult RegionUpdate(Region region)
        {
            var previousRegion = _db.Regionene
                .AsNoTracking()
                .Single(i => i.Id == region.Id);
            _db.Regionene.Update(region);
            var lstLags = _db.Lagene
                .AsNoTracking()
                .Where(i => i.Region == previousRegion.RegionNavn)
                .ToList();
            foreach (var lag in lstLags)
            {
                lag.Region = region.RegionNavn;
            }

            _db.Lagene.UpdateRange(lstLags);
            _db.SaveChanges();

            var redirectUrl = Url.Action("Admin", "Home");
            return Json(new { Url = redirectUrl });
        }

        [HttpDelete]
        public IActionResult RegionDelete(int id)
        {
            var region = _db.Regionene.Find(id);
            var lstLags = _db.Lagene
                .AsNoTracking()
                .Where(i => i.Region == region.RegionNavn)
                .ToList();
            _db.Lagene.RemoveRange(lstLags);
            _db.Regionene.Remove(region);
            _db.SaveChanges();

            var redirectUrl = Url.Action("Admin", "Home");
            return Json(new { Url = redirectUrl });
        }

        /// <summary>
        /// Get all lag where region is equal to param
        /// AsNoTracking for faster queries beacause we do not track queries which used to populate data
        /// </summary>
        /// <param name="region"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetLag([Bind(Prefix = "id")] string region)
        {
            var data = _db.Lagene
                .AsNoTracking()
                .Where(i => i.Region == region)
                .Select(i => new
                {
                    key = i.Id,
                    value = i.Navn
                })
                .ToList();
            return Ok(await Task.FromResult(data));
        }

        [HttpGet]
        public async Task<IActionResult> GetTeamUsers([FromQuery] string teamUser)
        {
            try
            {
                var data = _db.Users
                .AsNoTracking()
                .Where(i => i.FullName.Contains(teamUser) ||
                            i.UserName.Contains(teamUser))
                 .Select(i => new
                 {
                     Value = i.FullName
                 })
                 .GroupBy(i => i.Value)
                 .ToList();
                return Json(await Task.FromResult(data));
                //return Json(await Task.FromResult(data
                //    .GroupBy(i => i.Value)
                //    .Select(gr => gr.First())));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTeamUsersDetails([FromQuery] string teamUser)
        {
            try
            {
                return Json(await Task.FromResult(_db.Users
                    .AsNoTracking()
                    .Where(i => i.FullName.Contains(teamUser) ||
                                i.FullName == teamUser)
                    .Select(i => new
                    {
                        caption = i.FullName,
                        age = i.Age,
                        position = i.Position,
                        region = i.Region,
                        lag = i.Lag
                    })
                    .Single()));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public IActionResult MinProfil()
        {
            return View();
        }

        public IActionResult Nyheter()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Nyheter(Nyheter nyheter)
        {

            _db.Nyhetene.Add(new Nyheter(nyheter.PostTitle, nyheter.Message, User.Identity.Name));
            _db.SaveChanges();

            return View();
        }

        [HttpPost]
        public IActionResult UpdatePost(string postTitle, string message, int id)
        {

            _db.Nyhetene.Find(id).PostTitle = postTitle;
            _db.Nyhetene.Find(id).Message = message;
            _db.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IActionResult PostDelete(int id)
        {
            var post = _db.Nyhetene.Find(id);
            _db.Nyhetene.Remove(post);
            _db.SaveChanges();

            var redirectUrl = Url.Action("Nyheter", "Home");
            return Json(new { Url = redirectUrl });
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}

