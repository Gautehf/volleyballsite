using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VolleyballPage.Data;
using VolleyballPage.Models;
using System.Net.Http;
using VolleyballPage.Services;
using Microsoft.AspNetCore.Identity;

namespace VolleyballPage
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }
         public async Task CreateUsersAndRoles(IServiceScope serviceScope)
        {
            var userManager = serviceScope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
            var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();

            // First create the admin role
            await roleManager.CreateAsync(new IdentityRole("Admin"));

            // Then add one admin user
            var adminUser = new ApplicationUser { UserName = "admin@uia.no", Email = "admin@uia.no" };
            await userManager.CreateAsync(adminUser, "Password1.");
            await userManager.AddToRoleAsync(adminUser, "Admin");

            // Add one regular user
            var userUser = new ApplicationUser { UserName = "user@uia.no", Email = "user@uia.no" };
            await userManager.CreateAsync(userUser, "Password1.");
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();

                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                    
                    db.Database.EnsureDeleted();
                    db.Database.EnsureCreated();

                    // Add regular data here
                  /*  var LagSheet = "https://docs.google.com/spreadsheets/d/1KeFCQIr41arRo6rn4ROCuULVYxXjRn7pWgR7ZIzGyYo/export?format=csv";
                    
                    var Client = new HttpClient();
                    var Lag = Client.GetAsync(LagSheet).Result.Content.ReadAsStringAsync().Result;
                    
                    foreach(var s in Lag.Split('\n'))
                    {
                        db.Lagene.Add(new Models.Lag(s.Split(',')[0], s.Split(',')[1]));
                    }*/

                    db.Lagene.Add(new Models.Lag("Dristug", "Agder"));
                    db.Lagene.Add(new Models.Lag("GVK", "Agder"));
                    db.Lagene.Add(new Models.Lag("Tromsø BK", "Nord"));

                    db.Regionene.Add(new Models.Region("Agder"));
                    db.Regionene.Add(new Models.Region("Øst"));
                    db.Regionene.Add(new Models.Region("Trøndelag"));
                    db.Regionene.Add(new Models.Region("Rogaland"));
                    db.Regionene.Add(new Models.Region("Hordaland"));
                    db.Regionene.Add(new Models.Region("Nord"));
                    db.Regionene.Add(new Models.Region("Møre og Romsdal"));
                    db.Regionene.Add(new Models.Region("Sogn og fjordane"));

                    db.Nyhetene.Add(new Models.Nyheter("test1", "test1msg", "admin@uia.no"));
                    db.Nyhetene.Add(new Models.Nyheter("test2", "test2msg", "admin@uia.no"));
                    db.Nyhetene.Add(new Models.Nyheter("test3", "test3msg", "admin@uia.no"));


                    db.SaveChanges();
                    
                    // Then create the standard users and roles
                    CreateUsersAndRoles(serviceScope).Wait();
                }
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
