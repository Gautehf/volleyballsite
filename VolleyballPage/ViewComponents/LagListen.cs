using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VolleyballPage.Data;

namespace VolleyballPage.ViewComponents
{
    // Controller for the view component that shows a list of recent articles
    public class LagListen : ViewComponent
    {
        private readonly ApplicationDbContext _db;

        public LagListen(ApplicationDbContext db)
        {
            _db = db;
        
        }

        // This function fetches data for the view component.
        // It is an async function so it can run in parallell with other async functions.
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var gbPosts = await _db.Lagene.ToListAsync();
            gbPosts.Reverse();
            return View(gbPosts);
        }
    }
}