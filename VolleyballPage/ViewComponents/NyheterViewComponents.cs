using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VolleyballPage.Data;

namespace ViewComponents
{
    // Controller for the view component that shows a list of recent articles
    public class NyheterList : ViewComponent
    {
        private ApplicationDbContext _db;

        public NyheterList(ApplicationDbContext db)
        {
            _db = db;
        
        }

        // This function fetches data for the view component.
        // It is an async function so it can run in parallell with other async functions.
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var nyheter = await _db.Nyhetene.ToListAsync();
            nyheter.Reverse();
            return View(nyheter);
        }
    }
}